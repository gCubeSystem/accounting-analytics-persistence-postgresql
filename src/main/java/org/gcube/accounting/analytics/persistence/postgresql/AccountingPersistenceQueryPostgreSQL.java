/**
 * 
 */
package org.gcube.accounting.analytics.persistence.postgresql;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.OffsetDateTime;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.gcube.accounting.analytics.Filter;
import org.gcube.accounting.analytics.Info;
import org.gcube.accounting.analytics.NumberedFilter;
import org.gcube.accounting.analytics.TemporalConstraint;
import org.gcube.accounting.analytics.UsageValue;
import org.gcube.accounting.analytics.persistence.AccountingPersistenceBackendQuery;
import org.gcube.accounting.analytics.persistence.AccountingPersistenceBackendQueryConfiguration;
import org.gcube.accounting.analytics.persistence.AccountingPersistenceQuery;
import org.gcube.accounting.datamodel.aggregation.AggregatedServiceUsageRecord;
import org.gcube.accounting.datamodel.usagerecords.ServiceUsageRecord;
import org.gcube.accounting.persistence.AccountingPersistenceConfiguration;
import org.gcube.accounting.utility.postgresql.RecordToDBConnection;
import org.gcube.accounting.utility.postgresql.RecordToDBFields;
import org.gcube.accounting.utility.postgresql.RecordToDBMapping;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.documentstore.records.AggregatedRecord;
import org.gcube.documentstore.records.Record;
import org.gcube.documentstore.records.RecordUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class AccountingPersistenceQueryPostgreSQL implements AccountingPersistenceBackendQuery {
	
	protected static final Logger logger = LoggerFactory.getLogger(AccountingPersistenceQueryPostgreSQL.class);
	
	public static final String URL_PROPERTY_KEY = AccountingPersistenceConfiguration.URL_PROPERTY_KEY;
	
	public static final int DEFAULT_TOP_LIMIT = 10; 
	
	protected AccountingPersistenceBackendQueryConfiguration configuration;
	
	protected ObjectMapper objectMapper;
	
	protected Class<? extends AggregatedRecord<?, ?>> clz;
	protected TemporalConstraint temporalConstraint;
	
	protected Set<String> contexts;
	protected Collection<Filter> filters;
	
	protected RecordToDBMapping recordToDBMapping;
	
	static {
		// One Record per package is enough
		RecordUtility.addRecordPackage(ServiceUsageRecord.class.getPackage());
		RecordUtility.addRecordPackage(AggregatedServiceUsageRecord.class.getPackage());
	}
	
	public AccountingPersistenceQueryPostgreSQL() {
		this.objectMapper = new ObjectMapper();
		this.recordToDBMapping = new RecordToDBMapping();
	}
	
	@Override
	public void prepareConnection(AccountingPersistenceBackendQueryConfiguration configuration) throws Exception {
		this.configuration = configuration;
		Map<String, Class<? extends AggregatedRecord<?,?>>> aggregatedRecords = RecordUtility.getAggregatedRecordClassesFound();
		for(String typeName : aggregatedRecords.keySet()) {
			try {
				Class<? extends AggregatedRecord<?,?>> clz = aggregatedRecords.get(typeName); 
				recordToDBMapping.addRecordToDB(clz, configuration);
			} catch (Exception e) {
				new RuntimeException(e);
			}
		}
	}
	
	protected Connection getConnection(Class<? extends AggregatedRecord<?, ?>> clz) throws Exception {
		RecordToDBConnection recordDBConnection = recordToDBMapping.getRecordDBInfo(clz);
		if(recordDBConnection == null) {
			recordToDBMapping.addRecordToDB(clz, configuration);
			recordDBConnection = recordToDBMapping.getRecordDBInfo(clz);
		}
		return recordDBConnection.getConnection();
	}
	
	@Override
	public void setRequestedRecords(Class<? extends AggregatedRecord<?, ?>> clz) {
		this.clz = clz;
	}
	
	@Override
	public void setTemporalConstraint(TemporalConstraint temporalConstraint) {
		this.temporalConstraint = temporalConstraint;
	}
	
	@Override
	public void setContexts(Set<String> contexts) {
		this.contexts = contexts;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void setFilters(Collection<? extends Filter> filters) {
		this.filters = (Collection<Filter>) filters;
	}
	
	public RecordToDBMapping getRecordToDBMapping() {
		return recordToDBMapping;
	}

	protected void addProperty(ObjectNode objectNode, String key, Object value) {
		if(value instanceof Number) {
			
			if(value instanceof Integer) {
				objectNode.put(key, (int) value);
				return;
			}
			
			Long longValue = Long.valueOf(value.toString());
			objectNode.put(key, longValue);
			return;
		}
		
		objectNode.put(key, (String) value.toString());
		
	}
	
	protected SortedMap<Calendar, Info> getTimeSeries(Set<String> contexts) throws Exception {
		Connection connection = getConnection(clz);
		try {
			Statement statement = connection.createStatement();
			
			SortedMap<Calendar, Info> result = new TreeMap<>();
			
			Query query = new Query(clz);
			query.setTemporalConstraint(temporalConstraint);
			query.setFilters(filters);
			query.setContexts(contexts);
			
			String sql = query.getTimeSeriesQuery();
			
			Set<String> requestedTableField = query.getRequestedTableField();
			RecordToDBFields recordToDBMapper = query.getRecordToDBFields();
			
			logger.trace("Going to request the following query: {}", sql);
			ResultSet resultSet = statement.executeQuery(sql);
			
			while (resultSet.next()) {
				String tableFieldName = recordToDBMapper.getTableField(Query.DATE_OF_TIMESERIES_AS_FIELD);
				OffsetDateTime offsetDateTime = resultSet.getObject(tableFieldName, OffsetDateTime.class);
				Calendar calendar = getCalendar(offsetDateTime);
				
				ObjectNode objectNode = objectMapper.createObjectNode();
				
				for(String tableField : requestedTableField) {
					String usageRecordField = recordToDBMapper.getRecordField(tableField);
					Object obj = resultSet.getObject(tableField);
					addProperty(objectNode, usageRecordField, obj);
				}
				
				Info info = new Info(calendar, objectNode);
				result.put(calendar, info);
			}
			
			return result;
		}finally {
			connection.close();
		}
	}
	
	@Override
	public SortedMap<Calendar, Info> getTimeSeries() throws Exception {
		return getTimeSeries(contexts);
	}
	
	protected Calendar getCalendar(OffsetDateTime offsetDateTime) {
		Calendar calendar = Calendar.getInstance();
		long epochMillis = offsetDateTime.toInstant().toEpochMilli();
		calendar.setTimeInMillis(epochMillis);
		return calendar;
	}

	@Override
	public SortedMap<Filter, SortedMap<Calendar, Info>> getContextTimeSeries() throws Exception {
		
		SortedMap<Filter,SortedMap<Calendar,Info>> ret = new TreeMap<>();
		for(String context : contexts) {
			Filter contextFilter = new Filter("scope", context);
			Set<String> ctxs = new HashSet<>();
			ctxs.add(context);
			
			SortedMap<Calendar, Info> timeSeries = getTimeSeries(ctxs);
			if(!timeSeries.isEmpty()) {
				ret.put(contextFilter, timeSeries);
			}
		}
		return ret;
	}
	
	protected SortedSet<NumberedFilter> getNumberedValues(String key, String orderingProperty, Integer limit) throws Exception {
		Connection connection = getConnection(clz);
		try {
			Statement statement = connection.createStatement();
			
			if(orderingProperty == null) {
				orderingProperty = AccountingPersistenceQuery.getDefaultOrderingProperties(clz);
			}
			
			if(limit!= null && limit<1) {
				limit = DEFAULT_TOP_LIMIT;
			}
			
			SortedSet<NumberedFilter> result = new TreeSet<>();
			
			Query query = new Query(clz);
			query.setTemporalConstraint(temporalConstraint);
			query.setFilters(filters);
			query.setContexts(contexts);
			query.setTableFieldToRequest(key);
			query.setOrderByField(orderingProperty);
			query.setLimit(limit);
			
			String sql = query.getNextPossibleValueQuery();
			
			// List<String> requestedTableField = query.getRequestedTableField();
			RecordToDBFields recordToDBFields = query.getRecordToDBFields();
			
			logger.trace("Going to request the following query: {}", sql);
			ResultSet resultSet = statement.executeQuery(sql);
			
			String tableFieldORderingProperty = recordToDBFields.getTableField(orderingProperty);
			
			while (resultSet.next()) {
				String tableFieldKey = recordToDBFields.getTableField(key);
				Object value = resultSet.getObject(tableFieldKey);
				
				Object numberObject = resultSet.getObject(tableFieldORderingProperty);
				NumberedFilter numberedFilter = new NumberedFilter(key, value.toString(), (Number) numberObject, orderingProperty);
				result.add(numberedFilter);
			}
			return result;
			
		}finally {
			connection.close();
		}
	}
	
	@Override
	public SortedSet<NumberedFilter> getFilterValues(String key) throws Exception {
		return getNumberedValues(key, null, null);
	}

	@Override
	public SortedSet<NumberedFilter> getFilterValues(String key, Integer limit) throws Exception {
		return getNumberedValues(key, null, limit);
	}
	
	@Override
	public SortedMap<NumberedFilter, SortedMap<Calendar, Info>> getTopValues(String topKey, String orderingProperty, Integer limit)
			throws Exception {
		
		SortedMap<NumberedFilter,SortedMap<Calendar,Info>> ret = new TreeMap<>();
		
		SortedSet<NumberedFilter> top = getNumberedValues(topKey, orderingProperty, limit); 
		
		for(NumberedFilter numberedFilter : top) {
			filters.add(numberedFilter);
			SortedMap<Calendar,Info> map = getTimeSeries();
			ret.put(numberedFilter, map);
			filters.remove(numberedFilter);
		}
		
		return ret;
	}

	@Override
	public Record getRecord(String recordId, String type) throws Exception {
		Class<? extends AggregatedRecord<?, ?>> aggregatedRecordClass = RecordUtility.getAggregatedRecordClass(type);
		Connection connection = getConnection(aggregatedRecordClass);
		try {
			Statement statement = connection.createStatement();
			
			Query query = new Query(aggregatedRecordClass);
			query.setRecordId(recordId);
			String sql = query.getRecordQuery();
			
			RecordToDBFields recordToDBFields = query.getRecordToDBFields();
			
			ResultSet resultSet = statement.executeQuery(sql);
			resultSet.next();
			
			AggregatedRecord<?, ?> instance = aggregatedRecordClass.newInstance();
			Set<String> requiredFields = instance.getRequiredFields();
			
			for(String recordField : requiredFields) {
				String tableField = recordToDBFields.getTableField(recordField);
				Serializable serializable;
				switch (recordField) {
					case AggregatedRecord.START_TIME: case AggregatedRecord.END_TIME: case AggregatedRecord.CREATION_TIME:
						OffsetDateTime offsetDateTime = resultSet.getObject(tableField, OffsetDateTime.class);
						Calendar calendar = getCalendar(offsetDateTime);
						serializable = calendar.getTimeInMillis();
						break;
						
					default:
						serializable = resultSet.getObject(tableField).toString();
						break;
				}
				instance.setResourceProperty(recordField, serializable);
			}
			
			return instance;
			
		}finally {
			connection.close();
		}	
		
	}

	@Override
	public SortedMap<Filter, SortedMap<Calendar, Info>> getSpaceTimeSeries(Set<String> dataTypes) throws Exception {
		/*
		SortedMap<Filter, SortedMap<Calendar, Info>> sortedMap = new TreeMap<>();
		setRequestedRecords(AggregatedStorageStatusRecord.class);
		for(String dataType : dataTypes) {
			Filter filter = new Filter(StorageStatusRecord.DATA_TYPE, dataType);
			if(filters == null) {
				filters = new HashSet<>();
			}
			filters.add(filter);
			
			SortedMap<Calendar, Info> timeSeries = getTimeSeries();
			sortedMap.put(filter, timeSeries);
			
			filters.remove(filter);
		}
		return sortedMap;
		*/
		return null;
	}
	
	@Override
	public List<UsageValue> getUsageValueQuotaTotal(List<UsageValue> listUsage) throws Exception {
		return null;
	}
	
	
	@Override
	public void close() throws Exception {
		// OK
	}
	
	@Override
	public boolean isConnectionActive() throws Exception {
		return true;
	}
	
}
