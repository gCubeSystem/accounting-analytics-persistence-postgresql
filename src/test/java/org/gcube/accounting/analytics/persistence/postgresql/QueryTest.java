/**
 * 
 */
package org.gcube.accounting.analytics.persistence.postgresql;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.gcube.accounting.analytics.Filter;
import org.gcube.accounting.analytics.TemporalConstraint;
import org.gcube.accounting.analytics.TemporalConstraint.AggregationMode;
import org.gcube.accounting.analytics.persistence.AccountingPersistenceQuery;
import org.gcube.accounting.datamodel.UsageRecord;
import org.gcube.accounting.datamodel.aggregation.AggregatedServiceUsageRecord;
import org.gcube.accounting.datamodel.usagerecords.ServiceUsageRecord;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class QueryTest extends ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(QueryTest.class);
	
	public static final String[] users = new String[]{"luca.frosini", "lucio.lelii", "francesco.mangiacrapa", "fabio.sinibaldi", "massimiliano.assante", "giancarlo.panichi", "gianpaolo.coro"};
	
	private static final Random random;
	
	static {
		random = new Random();
	}
	
	public static String getRandomUser() {
		int randomNumber = random.nextInt(users.length);
		return users[randomNumber];
	}
	
	@Test
	public void testGetNoContextTimeSeriesQuery() throws Exception {
		Calendar startTimeCalendar = Calendar.getInstance();
		startTimeCalendar.set(Calendar.YEAR, 2020);
		startTimeCalendar.set(Calendar.MONTH, Calendar.MARCH);
		startTimeCalendar.set(Calendar.DAY_OF_MONTH, 1);
		startTimeCalendar.set(Calendar.HOUR_OF_DAY, 0);
		startTimeCalendar.set(Calendar.MINUTE, 0);
		
		Calendar entTimeCalendar = Calendar.getInstance();
		entTimeCalendar.set(Calendar.MONTH, Calendar.MARCH);
		entTimeCalendar.set(Calendar.DAY_OF_MONTH, 15);
		entTimeCalendar.set(Calendar.HOUR_OF_DAY, 16);
		entTimeCalendar.set(Calendar.MINUTE, 17);
		
		List<Filter> filters = new ArrayList<>();
		Filter filter = new Filter(UsageRecord.CONSUMER_ID, getRandomUser());
		filters.add(filter);
		filter = new Filter(UsageRecord.CONSUMER_ID, getRandomUser());
		filters.add(filter);
		
		TemporalConstraint temporalConstraint = new TemporalConstraint(startTimeCalendar.getTimeInMillis(), entTimeCalendar.getTimeInMillis(), AggregationMode.MONTHLY);
		Query query = new Query(AggregatedServiceUsageRecord.class);
		query.setTemporalConstraint(temporalConstraint);
		query.setFilters(filters);
		String ret = query.getTimeSeriesQuery();
		logger.debug(ret);
		
		
		Set<String> contexts = new HashSet<>();
		contexts.add(DEFAULT_TEST_SCOPE);
		contexts.add(ALTERNATIVE_TEST_SCOPE);
		query.setContexts(contexts);
		ret = query.getTimeSeriesQuery();
		logger.debug(ret);
		
		
		query = new Query(AggregatedServiceUsageRecord.class);
		query.setTemporalConstraint(temporalConstraint);
		query.setFilters(filters);
		query.setContexts(contexts);
		query.setTableFieldToRequest(ServiceUsageRecord.CALLED_METHOD);
		query.setOrderByField(AccountingPersistenceQuery.getDefaultOrderingProperties(AggregatedServiceUsageRecord.class));
		ret = query.getNextPossibleValueQuery();
		logger.debug(ret);
	}
	
	
	@Test
	public void testGetRecordQuery() throws Exception {
		Query query = new Query(AggregatedServiceUsageRecord.class);
		query.setRecordId("7c3fcb31-6909-451d-bae0-633b3bec0c21");
		String ret = query.getRecordQuery();
		logger.debug(ret);
	}

}
