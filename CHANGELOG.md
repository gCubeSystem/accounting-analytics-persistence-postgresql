This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Accounting Analytics Backend Connector for PostgreSQL

## [v2.0.1]

- Enhanced accounting-postgresql-utilities range 


## [v2.0.0]

- Added possibility of having multiple values for the same key which will be used in OR [#21353]


## [v1.0.1]

- Fixed bug on query


## [v1.0.0] [r5.2.0] - 2021-05-04

- First Release
